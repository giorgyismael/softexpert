﻿Create table UserSystem
(
	cod_user SERIAL not null,
	name_user varchar(100) not null,
	email_user varchar(50) not null,
	password_user varchar(75) not null,
	Constraint PK_UsersSystem Primary Key (cod_user)

);


Create table TaskUsers
(
	cod_task SERIAL not null,
	cod_task_from_user integer not null,
	cod_task_to_user integer not null,
	name_task varchar(50) not null,
	time_estimated varchar(50) not null,
	time_spent varchar(50),
	description_task varchar(500),
	active char(10),
	Constraint PK_TaskUsers Primary Key (cod_task),
	Constraint FK_FromUserSystem Foreign Key (cod_task_from_user) References UserSystem(cod_user),
	Constraint FK_ToUserSystem Foreign Key (cod_task_to_user) References UserSystem(cod_user)
);