<%@include file="template/headUser.jsp" %>
<body onload="Menu();defaultMessenger();">
    <%@include file="template/choiseNav.jsp" %>
    <div class="ui grid stackable" id="conteudo">
        <div class="six wide centered column">
            <h4 class="ui dividing header">
                <i class="add user icon"></i>
                Registro Usuario
            </h4>
            <form class="ui form" method="post" action="registerUser">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="name" pattern="[a-zA-Z][a-zA-Z\s]*" title="Preencha o campo com um valores Correspondentes ao seu nome."required="required" placeholder="Nome">
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="mail icon"></i>
                            <input type="email" name="email" required="required" placeholder="Email">
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" required="required" placeholder="Password">
                        </div>
                    </div>
                </div>
                <input type="submit" class="ui blue submit button" placeholder="Login">
                
                <%@include file="template/messengerError.jsp" %>
                <%@include file="template/messengerPositive.jsp" %>

            </form>

        </div>
    </div>
</body>

<%@include file="template/footerdefault.jsp" %>
