<%@include file="template/headUser.jsp" %>
<body onload="Menu();defaultMessenger();">
    <%@include file="template/islogin.jsp" %>
    <%@include file="template/choiseNav.jsp" %>
    <div class="ui grid stackable" id="conteudo">
        <div class="ten wide centered column">
            <h4 class="ui dividing header">
                <i class="list icon"></i>
                Lista de Tarefas
            </h4>
            <%@include file="template/resultListTask.jsp" %>
        </div>
    </div>
</body>
<%@include file="template/footerdefault.jsp" %>
