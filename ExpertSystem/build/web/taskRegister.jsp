<%@include file="template/headUser.jsp" %>
<body onload="Menu();defaultMessenger();">
    <%@include file="template/islogin.jsp" %>
    <%@include file="template/choiseNav.jsp" %>
    <div class="ui grid stackable" id="conteudo">
            <div class="ten wide centered column">
            <h4 class="ui dividing header">
                <i class="edit icon"></i>
                Cadastro de Tarefa
            </h4>
            <form class="ui form" method="post" action="taskRegister">
                <input type="hidden"  name="cod_task_from_user" value="${codigo}" readonly="readonly">
                <div class="field ">
                    <div class="ui left icon input">
                        <i class="remove bookmark icon"></i>
                        <input type="texte"  name="name_task" required="required" pattern="[a-zA-Z][a-zA-Z0-9\s]*" placeholder="Nome da Tarefa"readonly">
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text"  name="cod_user_from_user" value="${user}" readonly="readonly">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="wait icon"></i>
                            <input type="texte" name="time_estimated"  onfocus="(this.type = 'time')" onblur="(this.type = 'text')" onchange="calcPercentageDeviation()" onclick="calcPercentageDeviation()" id ="time_estimated" required="required" title="Preencha o campo com um valores num�ricos." pattern="[0-9]+$" placeholder="Tempo Total Estimado">
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui left icon input" style="display: none" id="div_time_spent">
                            <i class="line chart icon"></i>
                            <input type="text" name="time_spent" onfocus="(this.type = 'time')" onblur="(this.type = 'text')" onchange="calcPercentageDeviation();verifyLentimeSpent();" onclick="calcPercentageDeviation();" id="time_spent" placeholder="Tempo Gasto Total em (horas)" readonly="readonly">
                        </div>
                    </div>
                </div>
                <%@include file="template/UsersDropdown.jsp"%> 
                <div class="field">
                    <label>Descri��o da Tarefa</label>
                    <div class="ui left icon input">
                        <i class="icon"></i>
                        <textarea rows="3" name="descript_task"></textarea>
                    </div>
                </div>
                <%@include file="template/taskalter.jsp" %>
                <div class="ui buttons">
                    <button type="reset" class="ui button">Limpar</button>
                    <div class="or"></div>
                    <button type="submit" onclick="verifyTimeSpent();trsformTime();" class="ui positive button">Salvar</button>
                </div>
                <%@include file="template/messengerError.jsp" %>
                <%@include file="template/messengerPositive.jsp" %>
            </form>
        </div>
    </div>
</body>
<%@include file="template/footerdefault.jsp" %>