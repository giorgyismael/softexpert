<%-- 
    Document   : taskalter
    Created on : 17/04/2016, 22:14:00
    Author     : giorgy
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${empty param['id_task']}">
        <div style="display: none" class="field">
            <div class="ui checkbox">
                <input type="checkbox" onclick="percentageDeviation()" value="${task.activate.trim()}" hidden="" id="activate" name="activate" tabindex="0">
                <label>Finalizar Atividade em Amdamento</label>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="field">
            <div class="ui checkbox">
                <input type="checkbox" onclick="percentageDeviation()" value="${task.activate.trim()}" id="activate" name="activate" tabindex="0">
                <label>Finalizar Atividade em Amdamento</label>
            </div>
        </div>
    </c:otherwise>
</c:choose>
