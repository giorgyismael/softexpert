<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.UserSystem"%>
<div class="two fields">
     <div class="field">
    <div class="ui compact  menu">
        <div class="ui simple  dropdown item">
            <i class="users icon"></i>
            Resposável pela Tarefa
            <i class="dropdown green icon"></i>
            <div class="menu ">
                <select required="required" name="cod_task_to_user">
                    <c:forEach var="user" items= "${userList}" >
                        <option  value="${user.codigo}">${user.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>
     </div>


    <div class="field">
        <div class="ui left icon input " style="display: none" id="div_percentage_deviation">
            <i class="calculator outline icon"></i>
            <input type="text"  name="percentage_deviation" id="input_percentage_deviation" readonly="readonly" placeholder="Percentual de Desvio (Estimado x Esperado) %" >
        </div>
    </div>
</div>

    
