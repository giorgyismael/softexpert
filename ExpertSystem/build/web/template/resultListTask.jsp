<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="ui red table">
    <thead>
        <tr>
            <th class="five wide"><i class="remove ookmark icon"></i>Nome da Tarefa</th>
            <th class="tree wide"><i class="users icon"></i>Usuário Responsável</th>
            <th class="tree wide"><i class="unhide icon"></i>Ativo</th>
            <th class="right aligned"><i class="edit icon"></i>Editar</th>
        </tr>
    </thead>
        <tbody>
        <c:forEach var="task" items= "${taskList}" >
            <c:choose>
                <c:when test="${task.activate.trim() == 'on'}">
                    <tr class="negative">
                        <td class="five wide">${task.name_task}</td>
                        <td class="tree wide">${task.to_name_user}</td>
                        <td class="tree wide"><i class= " remove icon"></i></td>
                        <td class="right aligned"><i class="edit icon"></i><a href="taskRegister?id_task=${task.cod_task}">Edit</a></td>

                    </tr>
                </c:when>
                <c:otherwise>
                    <tr >
                        <td class="five wide">${task.name_task}</td>
                        <td class="tree wide">${task.to_name_user}</td>
                        <td class="tree wide"><i class="checkmark icon"></i></td>
                        <td class="right aligned"><i class="edit icon"></i><a href="taskRegister?id_task=${task.cod_task}">Edit</a></td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </tbody>
</table>