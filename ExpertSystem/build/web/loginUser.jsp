<%@include file="template/headUser.jsp" %>
<body onload="Menu();defaultMessenger();">
    <%@include file="template/choiseNav.jsp" %>
    <div class="ui grid stackable" id="conteudo">
        <div class="six wide centered column">
            <h4 class="ui dividing header">
                <i class="users icon"></i>
                Login
            </h4>


            <form class="ui form" method="post" action="loginUser">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="mail icon"></i>
                        <input type="email" name="email" placeholder="Email">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Password">
                    </div>
                </div>
                <input type="submit" class="ui blue submit button" placeholder="Login">

                <%@include file="template/messengerError.jsp" %>
                <%@include file="template/messengerPositive.jsp" %>

            </form>
        </div>
    </div>
</body>
<%@include file="template/footerdefault.jsp" %>