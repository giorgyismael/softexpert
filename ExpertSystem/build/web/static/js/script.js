function Menu() {
    try {
        pagina = document.getElementById("controle_menu").value;
        document.getElementById(pagina).className = 'item active';
    } catch (err) {
        document.getElementById("error_js").value = err.message;
    }
}

function defaultMessenger() {
    try {
        pagina = document.getElementById("default_messenger").value;
        if (pagina !== "null")
            alert(pagina);
    } catch (err) {
        document.getElementById("error_js").value = err.message;
    }
}



function percentageDeviation() {
    resultado = document.getElementById("activate").checked;
    input_percentage_deviation = document.getElementById("input_percentage_deviation");
    input_time_spent = document.getElementById("time_spent");
    input_time_estimated = document.getElementById("time_estimated");
    div_percentage_deviation = document.getElementById("div_percentage_deviation");
    div_time_spent = document.getElementById("div_time_spent");
    
    if (resultado) {

        div_time_spent.style.display = "";
        input_time_spent.required = true;
        input_time_spent.setAttribute("required", "required");
        input_time_spent.readOnly = false;


    } else {
        div_percentage_deviation.style.display = "none";
        div_time_spent.style.display = "none";
        input_time_spent.required = false;
        input_time_spent.value = "";
        input_time_estimated.value = "";
        input_percentage_deviation.value = "";
        input_time_spent.readOnly = true;
    }
}

function trsformTime(){
    document.getElementById("time_spent").type="time";
    document.getElementById("time_estimated").type="time";
    
    
}

function imprimeActive(){
    resultado = document.getElementById("activate");
    if (resultado.checked)
        resultado.setAttribute("value", "on");
    else
        resultado.setAttribute("value", "");
    
}

function verifyActionchecked(){
    resultado = document.getElementById("activate");
    if(resultado.value === "on"){
        resultado.checked="true";
        
        this.percentageDeviation();
        this.verifyLentimeSpent();
        this.calcPercentageDeviation();
        
        
    }
}

function verifyLentimeSpent(){
    input_time_spent = document.getElementById("time_spent");
    div_percentage_deviation = document.getElementById("div_percentage_deviation");
    
    if(input_time_spent !== "")
        div_percentage_deviation.style.display = "";
    else
        div_percentage_deviation.style.display = "none";
    
    
}

function verifyTimeSpent() {
    input_time_spent = document.getElementById("time_spent");
    if (!input_time_spent.required) {
        if (input_time_spent.value === "")
            input_time_spent.value = 0;
    }
}

function calcPercentageDeviation() {
    resultado = document.getElementById("activate").checked;
    input_time_spent = document.getElementById("time_spent");
    input_time_estimated = document.getElementById("time_estimated");
    input_percentage_deviation = document.getElementById("input_percentage_deviation");

    if (resultado)
        if (parseInt(input_time_spent.value) > 0 && (parseInt(input_time_estimated.value) > 0)) {
            result_percentage_deviation = ((parseInt(input_time_spent.value) * 100) / (parseInt(input_time_estimated.value)));
            input_percentage_deviation.value = result_percentage_deviation + "%";
            
        }
}

function PositionFooter() {
    var $footer = $("#footer"), footerHeight = $footer.height(),
            footerTop = ($(window).scrollTop() + $(window).height() - footerHeight) + "px";

    if (($(document.body).height() + footerHeight) < $(window).height()) {
        $footer.css({position: "absolute", top: footerTop});
    } else {
        $footer.css({position: "absolute"});
    }

    $footer.fadeTo(1000, 0.8);
}

function GerenciarFooter() {
    $(window).resize(PositionFooter)
    PositionFooter();
}

function errorMessenger(messengerError) {
    document.getElementById("errorMessenger").style.display = "block";
    if (messengerError == null)
        document.getElementById("errorMessenger").style.display = "none";
}
/*
 function dateStartTask() {
 var date = new Date();
 var day = date.getDate();
 var month = date.getMonth() + 1;
 var year = date.getFullYear();
 var today = year + "-" + month + "-" + day;
 document.getElementById('data_start_task').value = today;
 document.getElementById('data_start_task').min = today;
 }*/



$(document).ready(function () {
    $('.message .close')
            .on('click', function () {
                $(this)
                        .closest('.message')
                        .transition('fade')
                        ;
            })
            ;
    $('.tag.example .ui.dropdown')
            .dropdown({allowAdditions: true});

    $('.combo.dropdown')
            .dropdown({
                action: 'combo'
            })
            ;

    $('.link.example .dropdown')
            .dropdown({
                action: 'hide'
            })
            ;
    $('.ui.dropdown')
            .dropdown()
            ;

    $('.tag.example .ui.dropdown')
            .dropdown({
                allowAdditions: true
            })
            ;
    $('.restore.example .button')
            .on('click', function () {
                $('.restore.example .ui.dropdown')
                        .dropdown('restore defaults')
                        ;
            })
            ;
    $('#hybrid select')
            .dropdown({
                on: 'hover'
            })
            ;
    $('#search-select')
            .dropdown()
            ;
});