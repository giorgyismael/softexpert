/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.TaskUser;

/**
 *
 * @author giorgy
 */
public class taskUpdate extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TaskUser task = new TaskUser();
        task.setName_task(request.getParameter("name_task"));
        task.setCod_task(Integer.parseInt(request.getParameter("cod_task")));
        task.setFrom_user(Integer.parseInt(request.getParameter("cod_task_from_user")));
        task.setTo_user(Integer.parseInt((request.getParameter("cod_task_to_user"))));
        task.setTime_estimated(request.getParameter("time_estimated"));
        task.setActivate(request.getParameter("activate"));
        task.setTime_spent(request.getParameter("time_spent"));
        task.setDescription_task(request.getParameter("descript_task"));

        task.updateTask(task);
        
        request.setAttribute("titlePage", "Lista de Tarefas");
        request.setAttribute("default_messenger", "Alteraçção realializada com sucesso!");
        request.setAttribute("menu", "taskList");
        log(request.getSession().getAttribute("user") + " Alterou uma tarefa com código" + task.getCod_task());
        request.getRequestDispatcher("taskList.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
