/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.TaskUser;
import model.UserSystem;

/**
 *
 * @author giorgy
 */
public class taskList extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("titlePage", "Lista de Atividades");
        request.setAttribute("menu", "taskList");
        request.getRequestDispatcher("taskList.jsp").forward(request, response);

        List<TaskUser> taskList = new ArrayList<TaskUser>();
        TaskUser tasklist_users = new TaskUser();
        taskList = tasklist_users.listTask();

        request.getSession().setAttribute("taskList", taskList);
        request.getRequestDispatcher("taskList.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("titlePage", "Lista de Atividades");
        request.setAttribute("menu", "taskList");
        request.getRequestDispatcher("taskList.jsp").forward(request, response);

        List<TaskUser> taskList = new ArrayList<TaskUser>();
        TaskUser tasklist_users = new TaskUser();
        taskList = tasklist_users.listTask();

        request.getSession().setAttribute("taskList", taskList);
        request.getRequestDispatcher("taskList.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
