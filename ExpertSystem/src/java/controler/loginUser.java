package controler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.UserSystem;


public class loginUser extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("titlePage", "Login do Usuario");
        request.setAttribute("menu", "loginUser");
        request.getRequestDispatcher("loginUser.jsp").forward(request, response);
     }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        UserSystem login_user = new UserSystem();
        login_user.setEmail(request.getParameter("email"));
        login_user.setPassword(request.getParameter("password"));
        
        if (login_user.loginUser(login_user)){
            request.setAttribute("titlePage", "Cadastro Tarefa");
            request.setAttribute("menu", "taskRegister");
            
            HttpSession session = request.getSession();
            session.setAttribute("user", login_user.getName());
            session.setAttribute("codigo", login_user.getCodigo());
            log(login_user.getName() + " Login in System");
            
             List<UserSystem> userList = new ArrayList<UserSystem>();
            UserSystem users = new UserSystem();
            userList = users.listUsers();

            request.getSession().setAttribute("userList", userList);
            request.getRequestDispatcher("taskRegister.jsp").forward(request, response);
         }
        else{
            request.setAttribute("messengerNegative", "Usuario ou senha invalido!");
            log(login_user.getName() + " Error Login in System");
            request.getRequestDispatcher("loginUser.jsp").forward(request, response);
            
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
