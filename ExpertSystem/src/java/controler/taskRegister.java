package controler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.TaskUser;
import model.UserSystem;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import javafx.beans.binding.Bindings;

/**
 *
 * @author giorgy
 */
public class taskRegister extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameterMap().containsKey("id_task")) {
            int id_task = Integer.parseInt(request.getParameter("id_task"));
            TaskUser task = new TaskUser();
            task = task.searchTask(id_task);
                        
            List<UserSystem> userList = new ArrayList<UserSystem>();
            UserSystem users = new UserSystem();
            userList = users.listUsers();

            request.getSession().setAttribute("userList", userList);
            request.getSession().setAttribute("task", task);
            request.setAttribute("titlePage", "Editar Registro");
            request.setAttribute("menu", "taskRegister");
            request.getRequestDispatcher("taskUpdate.jsp").forward(request, response);
            
            
        } else {
            request.setAttribute("titlePage", "Registro de Atividades");
            request.setAttribute("menu", "taskRegister");

            List<UserSystem> userList = new ArrayList<UserSystem>();
            UserSystem users = new UserSystem();
            userList = users.listUsers();

            request.getSession().setAttribute("userList", userList);
            request.getRequestDispatcher("taskRegister.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        TaskUser task = new TaskUser();
        task.setName_task(request.getParameter("name_task"));
        task.setFrom_user(Integer.parseInt(request.getParameter("cod_task_from_user")));
        task.setTo_user(Integer.parseInt((request.getParameter("cod_task_to_user"))));
        task.setTime_estimated(request.getParameter("time_estimated"));
        task.setActivate(request.getParameter("activate"));
        task.setTime_spent(request.getParameter("time_spent"));

        if (request.getParameter("descript_task") != null) {
            task.setDescription_task(request.getParameter("descript_task"));
        }

        task.inserTask(task);

        request.setAttribute("titlePage", "Lista de tarefas");
        request.setAttribute("default_messenger", "Tarafa Cadastrada com Sucesso!");
        request.setAttribute("menu", "taskRegister");
        log(request.getSession().getAttribute("user") + " Registou uma tarefa");
        request.getRequestDispatcher("taskRegister.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
