/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.UserSystem;


public class registerUser extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("titlePage", "Registro Usuario");
        request.setAttribute("menu", "registerUser");
        request.getRequestDispatcher("registerUser.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserSystem newuser = new UserSystem();
        newuser.setName(request.getParameter("name"));
        newuser.setEmail(request.getParameter("email"));
        newuser.setPassword(request.getParameter("password"));

         if (newuser.validUser(newuser)){
             request.setAttribute("messengerNegative", "Ops... Parece que este e-mail já foi cadsatrado. \nTente acessar com outro email, ou entre em contato com Administrador!");
             request.setAttribute("menu", "registerUser");
             request.getRequestDispatcher("registerUser.jsp").forward(request, response);
             log(newuser.getName() + " Error Register in System");
         }
         else{
             newuser.insertUser(newuser);
             request.setAttribute("titlePage", "Login Usuario");
             request.setAttribute("messengerPositive", "Cadastro Realizado.");
             request.setAttribute("menu", "loginUser");
             log(newuser.getName() + " Registred in System");
             request.getRequestDispatcher("loginUser.jsp").forward(request, response);
         }
       
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
