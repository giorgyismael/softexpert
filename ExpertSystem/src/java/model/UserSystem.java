package model;
import conectionsFactory.conectionsFactory;
import conectionsFactory.connectionsUserSystem;
import java.io.Serializable;
import static java.rmi.server.LogStream.log;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UserSystem implements Serializable, connectionsUserSystem{
    private
           String codigo; 
           String name;
           String email;
           String password;
           

    public UserSystem() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public boolean validLogin(String user, String password){
        if (user.equals("admin"))
            return true;
        else
            return false;
    }
    
     
    public Connection connectionsDb(){
       return conectionsFactory.getConnection();
    }

    @Override
    public boolean insertUser(UserSystem user) {
        try {
  
            Connection connections = this.connectionsDb();
            String sql = "INSERT INTO UserSystem(name_user, email_user, password_user) VALUES (?,?,?)";
            
            PreparedStatement insertSt = connections.prepareStatement(sql);
            insertSt.setString(1, user.getName());
            insertSt.setString(2, user.getEmail());
            insertSt.setString(3, user.getPassword());
            insertSt.executeUpdate();
            insertSt.close();
            connections.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
  
       
    }

    @Override
    public void updateUser(UserSystem user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeUser(UserSystem user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validUser(UserSystem user) {
        boolean achou = false;
        try {
            Connection connections = this.connectionsDb();
            String sql = "Select * from usersystem where email_user = ?";
            
            PreparedStatement insertSt = connections.prepareStatement(sql);
            insertSt.setString(1, user.getEmail());
            ResultSet result = insertSt.executeQuery();
            if (result.next())
                achou = true;
            insertSt.close();
            connections.close();
            return achou;
        } catch (Exception ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return achou;
}    

    @Override
    public boolean loginUser(UserSystem user) {
        boolean achou = false;
        try {
            Connection connections = this.connectionsDb();
            String sql = "Select * from usersystem where email_user = ?";
            
            PreparedStatement insertSt = connections.prepareStatement(sql);
            insertSt.setString(1, user.getEmail());
            ResultSet result = insertSt.executeQuery();
            if (result.next()){
                user.setName(result.getString("name_user"));
                user.setCodigo(result.getString("cod_user"));
                if (result.getString("password_user").equals(user.getPassword()))
                    achou = true;
            }
            insertSt.close();
            connections.close();
            return achou;
            
        } catch (Exception ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return achou;
   }

    @Override
    public List<UserSystem> listUsers() {
        List<UserSystem> users = new ArrayList<>();
        try {
            Connection connections = this.connectionsDb();
            String sql = "select * from UserSystem order by name_user asc";
            Statement insertSt = connections.createStatement();
            ResultSet result = insertSt.executeQuery(sql);
            
            while(result.next()){
                UserSystem user = new UserSystem();
                user.setCodigo(result.getString("cod_user"));
                user.setName(result.getString("name_user"));
                user.setEmail(result.getString("email_user"));
                user.setPassword(result.getString("password_user"));
                users.add(user);
            }
            insertSt.close();
            connections.close();
        } catch (Exception ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }
    public String searchUser(int id_user){
        String name_user = "";
        try {
            Connection connections = this.connectionsDb();
            String sql = "select name_user from UserSystem where cod_user=?";
            PreparedStatement insertSt = connections.prepareStatement(sql);
            insertSt.setInt(1, id_user);
            ResultSet result = insertSt.executeQuery();
            
            result.next();
            name_user = result.getString("name_user");
            
            insertSt.close();
            connections.close();
        } catch (Exception ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return name_user;
    }
 
    
}
