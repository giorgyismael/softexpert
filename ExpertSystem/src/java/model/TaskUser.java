package model;

import conectionsFactory.conectionsFactory;
import conectionsFactory.connectionsTaskUser;
import java.util.Date;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaskUser implements Serializable, connectionsTaskUser {

    private int cod_task;
    String name_task;
    int from_user;
    int to_user;
    String time_estimated;
    String time_spent;
    String activate;
    String description_task;
    String to_name_user;

    public TaskUser() {

    }

    public String getTo_name_user() {
        return to_name_user;
    }

    public void setTo_name_user(String to_name_user) {
        this.to_name_user = to_name_user;
    }

    public int getCod_task() {
        return cod_task;
    }

    public void setCod_task(int cod_task) {
        this.cod_task = cod_task;
    }

    public String getName_task() {
        return name_task;
    }

    public void setName_task(String name_task) {
        this.name_task = name_task;
    }

    public int getFrom_user() {
        return from_user;
    }

    public void setFrom_user(int from_user) {
        this.from_user = from_user;
    }

    public int getTo_user() {
        return to_user;
    }

    public void setTo_user(int to_user) {
        this.to_user = to_user;
    }

    public String getTime_estimated() {
        return time_estimated;
    }

    public void setTime_estimated(String time_estimated) {
        this.time_estimated = time_estimated;
    }

    public String getTime_spent() {
        return time_spent;
    }

    public void setTime_spent(String time_spent) {
        this.time_spent = time_spent;
    }

    public String getDescription_task() {
        return description_task;
    }

    public void setDescription_task(String description_task) {
        this.description_task = description_task;
    }

    public String getActivate() {
        return activate;
    }

    public void setActivate(String activate) {
        this.activate = activate;
    }

    public Connection connectionsDb() {
        return conectionsFactory.getConnection();
    }

    @Override
    public boolean inserTask(TaskUser task) {
        try {

            Connection connections = this.connectionsDb();
            String sql = "INSERT INTO TaskUsers(cod_task_from_user, cod_task_to_user,name_task, time_estimated,time_spent,description_task, active)"
                    + " VALUES (?,?,?,?,?,?,?)";

            PreparedStatement insertSt = connections.prepareStatement(sql);
            insertSt.setInt(1, task.getFrom_user());
            insertSt.setInt(2, task.getTo_user());
            insertSt.setString(3, task.getName_task());
            insertSt.setString(4, task.getTime_estimated());
            insertSt.setString(5, task.getTime_spent());
            insertSt.setString(6, task.getDescription_task());
            insertSt.setString(7, task.getActivate());
            insertSt.executeUpdate();

            insertSt.close();
            connections.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    public void updateTask(TaskUser task) {
        try {
            Connection connections = this.connectionsDb();
            String sql = "UPDATE TaskUsers SET cod_task_from_user=?, "
                    + "cod_task_to_user=?, name_task=?, time_estimated=?,"
                    + "time_spent=?,description_task=?, active=? WHERE cod_task=?";

            PreparedStatement insertSt = connections.prepareStatement(sql);
            insertSt.setInt(1, task.getFrom_user());
            insertSt.setInt(2, task.getTo_user());
            insertSt.setString(3, task.getName_task());
            insertSt.setString(4, task.getTime_estimated());
            insertSt.setString(5, task.getTime_spent());
            insertSt.setString(6, task.getDescription_task());
            insertSt.setString(7, task.getActivate());
            insertSt.setInt(8, task.getCod_task());
            insertSt.executeUpdate();
            insertSt.close();
            connections.close();
            
        }  catch (Exception ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void removeTask(TaskUser task) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TaskUser searchTask(int id_task) {
        TaskUser task = new TaskUser();
        try {
            Connection connections = this.connectionsDb();
            String sql = "select cod_task, cod_task_from_user,cod_task_to_user, "
                    + "name_task,time_estimated,time_spent,description_task,active "
                    + "from TaskUsers where cod_task=?";
            PreparedStatement insertSt = connections.prepareStatement(sql);
            insertSt.setInt(1, id_task);
            ResultSet result = insertSt.executeQuery();

            result.next();
            task.setActivate(result.getString("active"));
            task.setDescription_task(result.getString("description_task"));
            task.setFrom_user(result.getInt("cod_task_from_user"));
            task.setName_task(result.getString("name_task"));
            task.setTime_estimated(result.getString("time_estimated"));
            task.setTime_spent(result.getString("time_spent"));
            task.setTo_user(result.getInt("cod_task_to_user"));
            task.setCod_task(result.getInt("cod_task"));
            
            insertSt.close();
            connections.close();

        } catch (Exception ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return task;

    }

    @Override
    public List<TaskUser> listTask() {
        List<TaskUser> list_task = new ArrayList<>();
        try {
            Connection connections = this.connectionsDb();
            String sql = "select * from TaskUsers order by active desc, name_task asc";
            Statement insertSt = connections.createStatement();
            ResultSet result = insertSt.executeQuery(sql);
            UserSystem user = new UserSystem();

            while (result.next()) {
                TaskUser task = new TaskUser();
                task.setActivate(result.getString("active"));
                task.setDescription_task(result.getString("description_task"));
                task.setFrom_user(result.getInt("cod_task_from_user"));
                task.setName_task(result.getString("name_task"));
                task.setTime_estimated(result.getString("time_estimated"));
                task.setTime_spent(result.getString("time_spent"));
                task.setTo_user(result.getInt("cod_task_to_user"));
                task.setCod_task(result.getInt("cod_task"));
                task.setTo_name_user(user.searchUser(result.getInt("cod_task_to_user")));
                list_task.add(task);
            }

            insertSt.close();
            connections.close();

        } catch (Exception ex) {
            Logger.getLogger(UserSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list_task;
    }

    private java.sql.Date getFish_task() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
