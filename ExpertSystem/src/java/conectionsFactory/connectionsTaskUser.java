/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conectionsFactory;

import java.util.List;
import model.TaskUser;

/**
 *
 * @author giorgy
 */
public interface connectionsTaskUser {
    public boolean inserTask(TaskUser task);
    public void updateTask(TaskUser task);
    public void removeTask(TaskUser task);
    public TaskUser searchTask(int id_task);
    public List<TaskUser> listTask();
    
}
