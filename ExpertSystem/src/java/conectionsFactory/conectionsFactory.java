/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conectionsFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author giorgy
 */
public class conectionsFactory {
    public static Connection getConnection(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(conectionsFactory.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Driver Postgres Não encontrado", ex);
            
        }
        try {
            return DriverManager.getConnection("jdbc:postgresql://localhost:5432/ExpertSystem", "postgres", "postgres");
                    } catch (SQLException ex) {
            Logger.getLogger(conectionsFactory.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Erro ao Abrir Conexão com Baco de dados", ex);
        }
    };
    
}
