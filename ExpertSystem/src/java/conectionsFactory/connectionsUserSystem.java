/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conectionsFactory;
import static java.util.Collections.list;
import java.util.List;
import model.UserSystem;

/**
 *
 * @author giorgy
 */
public interface connectionsUserSystem {
    public boolean insertUser(UserSystem user);
    public String searchUser(int id_user);
    public void updateUser(UserSystem user);
    public void removeUser(UserSystem user);
    public boolean validUser(UserSystem user);
    public boolean loginUser(UserSystem user);
    public List<UserSystem> listUsers();
    
}
