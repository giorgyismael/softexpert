<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">        
        
        <link rel="stylesheet" href="static/css/semantic/semantic.css" />
        <link rel="stylesheet" href="static/css/base.css" />
        <link rel="stylesheet" href="static/css/semantic/components/dropdown.css" />

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script src="static/js/semantic/semantic.js"></script>
        <script src="static/js/script.js"></script>
        <script src="static/js/jquery.js"></script>
        <script src="static/js/semantic/dropdown.js"></script>
        
        
        <title><%=request.getAttribute("titlePage")%></title>
    </head>
