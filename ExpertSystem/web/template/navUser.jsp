<nav id="header" class="ui inverted segment">
    <nav class="ui inverted secondary pointing menu">
        <a href="taskRegister" id="taskRegister" class="item">
            <i class="edit icon"></i>
            Cadastar Tarefa
        </a>
        <a href="taskList" id="taskList" class="item">
            <i class="list Layout icon"></i>
            Lista Tarefas
        </a>
        <a href="https://giorgyismael@bitbucket.org/giorgyismael/softexpert.git" id="git" class="item">
            <i class="bitbucket icon"></i>
            Git
        </a>
        <div class="right menu">

            <div class="item">
                <div class="ui simple dropdown" id="size_name_user">
                    <i class="user icon"></i>
                    Ol�  <i class="dropdown icon">${user}</i>
                    <div id='menu_user' class="menu">
                    <div id="logout" class="item "><i class="sign out icon"></i><a href="logout">Logout</a></div>
                    </div>
                </div>
            </div>
        </div>

        <input id="controle_menu" value="<%=request.getAttribute("menu")%>" type="hidden">
        <input id="error_js" type="hidden">
        <input id="default_messenger" type="hidden" value="<%=request.getAttribute("default_messenger")%>">
    </nav>
</nav>

