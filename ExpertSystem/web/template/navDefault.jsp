<nav id="header" class="ui inverted segment">
    <nav class="ui inverted secondary pointing menu">
        <a href="index" id="index" class="item">
            <i class="home icon"></i>
            ExpertSystem
        </a>
        
         <a href="registerUser" id="registerUser" class="item">
            <i class="add user icon"></i>
                Registre-se
        </a>
        <a href="https://giorgyismael@bitbucket.org/giorgyismael/softexpert.git" id="git" class="item">
            <i class="bitbucket icon"></i>
            Git
        </a>
        <div class="right menu">
            <a href="loginUser" id="loginUser" class="item">
                <i class="sign in icon"></i>
                Entrar
            </a>
        </div>
        <input id="controle_menu" value="<%=request.getAttribute("menu")%>" type="hidden">
        <input id="default_messenger" type="hidden" value="<%=request.getAttribute("default_messenger")%>">
        <input id="error_js" type="hidden">
    </nav>
</nav>

